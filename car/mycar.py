from car.direction import *
import time
import cv2 as cv
from pyzbar.pyzbar import decode
import numpy as np
import picamera
import io


BLOCK1 = 'Block-2'
BLOCK2 = 'Block-1'
DOOR1 = 'Door2'
DOOR2 = 'Door1'

NONE = -1
LEFT_SMALL = 0
LEFT_BIG = 1
CENTER = 2
RIGHT_SMALL = 3
RIGHT_BIG = 4
CLOSE = 5

STATUS_IDLE = 100
STATUS_FINDING_BLOCK1 = 101
STATUS_FINDING_DOOR1 = 102
STATUS_FINDING_BLOCK2 = 103
STATUS_FINDING_DOOR2 = 104

SCAN_ENABLE = False

car_status = STATUS_IDLE
camera = None
is_run = False
qcode = None


def init():
    GPIO.setmode(GPIO.BOARD)

    GPIO.setup(R_PWM, GPIO.OUT)
    GPIO.setup(R_FORWARD_EN, GPIO.OUT)
    GPIO.setup(R_BACKWARD_EN, GPIO.OUT)
    GPIO.setup(L_PWM, GPIO.OUT)
    GPIO.setup(L_FORWARD_EN, GPIO.OUT)
    GPIO.setup(L_BACKWARD_EN, GPIO.OUT)

    GPIO.setup(L_HIT_PIN, GPIO.IN)
    GPIO.setup(C_HIT_PIN, GPIO.IN)
    GPIO.setup(R_HIT_PIN, GPIO.IN)


def scan_block2():
    global car_status
    car_status = STATUS_FINDING_BLOCK2
    print 'scan_block2'
    global qcode
    qcode = BLOCK2


def check_close():
    global car_status
    if car_status == STATUS_FINDING_DOOR1:
        print 'STATUS_FINDING_DOOR1'
        backward(2.5)
        do_circle(0.9)
        forward(1.4)
        scan_block2()
    elif car_status == STATUS_FINDING_DOOR2:
        print 'STATUS_FINDING_DOOR2'
        global SCAN_ENABLE
        SCAN_ENABLE = False
        stop()


def check_image_direct(o_width, q_left, q_width):
    if q_width > 110 and (car_status == STATUS_FINDING_DOOR1 or car_status == STATUS_FINDING_DOOR2):
        return CLOSE

    q_center = q_left + q_width/2
    o_center = o_width/2
    diff = q_center - o_center

    if diff <= -50:
        if q_width > 50:
            return LEFT_SMALL
        else:
            return LEFT_BIG
    elif diff >= 50:
        if q_width > 50:
            return RIGHT_SMALL
        else:
            return RIGHT_BIG
    return CENTER


def has_qcode(orgin, result, target_code):
    print ' target: ', target_code, result
    for decode_ in result:
        decode_str = decode_.data
        if decode_str != target_code:
            print decode_str, 'continue'
            continue
        rect = decode_.rect
        q_left, q_width = rect.left, rect.width
        o_width = 640
        res = check_image_direct(o_width, q_left, q_width)
        print res
        return res
    return NONE


def print_time(start_time):
    print 'time: ', time.time() - start_time


def scan_qcode():
    global qcode
    global SCAN_ENABLE
    SCAN_ENABLE = True
    count = 0
    global camera
    while SCAN_ENABLE:
        # if count > 5:
        #     break
        temp_code = qcode
        count += 1
        start_time = time.time()
        stream = io.BytesIO()
        camera.capture(stream, format='jpeg')
        data = np.fromstring(stream.getvalue(), dtype=np.uint8)
        image = cv.imdecode(data, 1)
        decoded = decode(image)
        if qcode!= temp_code:
            continue
        has_code = has_qcode(image, decoded, qcode)
        if has_code == CLOSE:
            print_time(start_time)
            print 'check_close'
            check_close()
        elif has_code == LEFT_SMALL:
            print_time(start_time)
            print 'turn_left'
            turn_left(0.3)
        elif has_code == LEFT_BIG:
            print_time(start_time)
            print 'turn_left'
            turn_left(0.6)
        elif has_code == RIGHT_SMALL:
            print_time(start_time)
            print 'turn_right'
            turn_right(0.3)
        elif has_code == RIGHT_BIG:
            print_time(start_time)
            print 'turn_right'
            turn_right(0.6)
        elif has_code == CENTER:
            print_time(start_time)
            print 'forward'
            forward(0.6)
        else:
            print_time(start_time)
            print 'do_circle'
            do_circle(0.2)


def block_hit_callback(self):
    global car_status
    print 'block_hit_callback', str(car_status)
    if car_status == STATUS_FINDING_DOOR1 or car_status == STATUS_FINDING_DOOR2 :
        return
    global qcode
    if car_status == STATUS_FINDING_BLOCK1:
        shake_right(0.8)
        do_circle(2)
        car_status = STATUS_FINDING_DOOR1
        qcode = DOOR1
    elif car_status == STATUS_FINDING_BLOCK2:
        shake_right(0.8)
        do_circle(2)
        car_status = STATUS_FINDING_DOOR2
        qcode = DOOR2


def scan_block1():
    GPIO.add_event_detect(C_HIT_PIN, GPIO.RISING, callback=block_hit_callback, bouncetime=200)
    GPIO.add_event_detect(L_HIT_PIN, GPIO.RISING, callback=block_hit_callback, bouncetime=200)
    GPIO.add_event_detect(R_HIT_PIN, GPIO.RISING, callback=block_hit_callback, bouncetime=200)
    global car_status
    car_status = STATUS_FINDING_BLOCK1
    print 'scan_block1'
    global qcode
    qcode = BLOCK1
    scan_qcode()


def start_scan():
    global camera
    camera = picamera.PiCamera()
    camera.resolution = (640, 480)
    camera.start_preview()
    time.sleep(2)
    scan_block1()
    # forward(100)


# def start():
#     turn_left(0)
#     mv = cv.VideoCapture(0)
#     has_frame = False
#     frame_1 = None
#     start_time = None
#     while True:
#         ret, frame = mv.read()
#         if has_frame:
#             gray_frame = cv.flip(frame, 1)
#             print str(time.time()-start_time)
#             if time.time()-start_time > 500 and match(frame_1, gray_frame):
#                 time.sleep(0.2)
#                 stop()
#                 mv.release()
#                 cv.destroyAllWindows()
#                 break
#         else:
#             frame_1 = cv.flip(frame, 1)
#             has_frame = True
#             start_time = time.time()
#             cv.imshow("video", frame_1)
#         time.sleep(0.1)
