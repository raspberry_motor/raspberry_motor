import RPi.GPIO as GPIO
from car.pin import *
import time


def forward(last_time):
    print 'call start'
    GPIO.output(L_FORWARD_EN, GPIO.HIGH)
    GPIO.output(L_BACKWARD_EN, GPIO.LOW)
    # GPIO.output(L_PWM, GPIO.HIGH)
    l_pwm = GPIO.PWM(L_PWM, 100)
    l_pwm.start(50)

    GPIO.output(R_FORWARD_EN, GPIO.HIGH)
    GPIO.output(R_BACKWARD_EN, GPIO.LOW)
    # GPIO.output(R_PWM, GPIO.HIGH)
    r_pwm = GPIO.PWM(R_PWM, 100)
    r_pwm.start(50)

    time.sleep(last_time)
    l_pwm.stop()
    r_pwm.stop()



def backward(last_time):
    GPIO.output(L_FORWARD_EN, GPIO.LOW)
    GPIO.output(L_BACKWARD_EN, GPIO.HIGH)
    l_pwm = GPIO.PWM(L_PWM, 100)
    l_pwm.start(50)

    GPIO.output(R_FORWARD_EN, GPIO.LOW)
    GPIO.output(R_BACKWARD_EN, GPIO.HIGH)
    r_pwm = GPIO.PWM(R_PWM, 100)
    r_pwm.start(50)

    time.sleep(last_time)
    l_pwm.stop()
    r_pwm.stop()


def stop():
    print 'call stop'
    GPIO.output(L_FORWARD_EN, GPIO.LOW)
    GPIO.output(L_BACKWARD_EN, GPIO.LOW)

    GPIO.output(R_FORWARD_EN, GPIO.LOW)
    GPIO.output(R_BACKWARD_EN, GPIO.LOW)

    GPIO.cleanup()


def turn_left(last_time):
    GPIO.output(L_FORWARD_EN, GPIO.HIGH)
    GPIO.output(L_BACKWARD_EN, GPIO.LOW)
    l_pwm = GPIO.PWM(L_PWM, 100)
    l_pwm.start(40)

    GPIO.output(R_FORWARD_EN, GPIO.HIGH)
    GPIO.output(R_BACKWARD_EN, GPIO.LOW)
    r_pwm = GPIO.PWM(R_PWM, 100)
    r_pwm.start(50)

    time.sleep(last_time)
    l_pwm.stop()
    r_pwm.stop()



def turn_right(last_time):
    GPIO.output(L_FORWARD_EN, GPIO.HIGH)
    GPIO.output(L_BACKWARD_EN, GPIO.LOW)
    l_pwm = GPIO.PWM(L_PWM, 100)
    l_pwm.start(50)

    GPIO.output(R_FORWARD_EN, GPIO.HIGH)
    GPIO.output(R_BACKWARD_EN, GPIO.LOW)
    r_pwm = GPIO.PWM(R_PWM, 100)
    r_pwm.start(40)

    time.sleep(last_time)
    l_pwm.stop()
    r_pwm.stop()


def shake_right(last_time):
    GPIO.output(L_FORWARD_EN, GPIO.HIGH)
    GPIO.output(L_BACKWARD_EN, GPIO.LOW)
    l_pwm = GPIO.PWM(L_PWM, 100)
    l_pwm.start(50)

    GPIO.output(R_FORWARD_EN, GPIO.HIGH)
    GPIO.output(R_BACKWARD_EN, GPIO.LOW)
    r_pwm = GPIO.PWM(R_PWM, 100)
    r_pwm.start(40)

    time.sleep(last_time)
    l_pwm.stop()
    r_pwm.stop()


def do_circle(last_time):
    GPIO.output(L_FORWARD_EN, GPIO.LOW)
    GPIO.output(L_BACKWARD_EN, GPIO.LOW)
    GPIO.output(L_PWM, GPIO.LOW)


    GPIO.output(R_FORWARD_EN, GPIO.HIGH)
    GPIO.output(R_BACKWARD_EN, GPIO.LOW)
    r_pwm = GPIO.PWM(R_PWM, 100)
    r_pwm.start(50)

    time.sleep(last_time)
    r_pwm.stop()